using ToDoList.Data.Entities;

namespace ToDoList.Data.Repositories;

public interface IToDoCollectionRepository
{
    List<ToDoCollection> GetAll();
    ToDoCollection Get(int id);
    void Insert(ToDoCollection collection);
    void Update(ToDoCollection collection);
    void Delete(int id);
}