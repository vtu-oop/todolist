using ToDoList.Data.Entities;
using System.Data;
using System.Data.SqlClient;


namespace ToDoList.Data.Repositories;

public class ToDoCollectionRepository : IToDoCollectionRepository
{
    private readonly string connectionString;

    public ToDoCollectionRepository(string connectionString)
    {
        this.connectionString = connectionString;
    }
    
    public List<ToDoCollection> GetAll()
    {
        IDbConnection connection = new SqlConnection(connectionString);

        List<ToDoCollection> resultSet = new List<ToDoCollection>();
        using (connection)
        {
            connection.Open();
            IDbCommand command = connection.CreateCommand();
            command.CommandText = 
@"SELECT * FROM ToDoCollection";

            IDataReader reader = command.ExecuteReader();
            using (reader)
            {
                while (reader.Read())
                {
                    ToDoCollection collection = new ToDoCollection();
                    collection.Id = (int) reader["Id"];
                    collection.Title = (string) reader["Title"];
                    
                    resultSet.Add(collection);
                }
            }
        }

        return resultSet;
    }

    public ToDoCollection Get(int id)
    {
        throw new NotImplementedException();
    }

    public void Insert(ToDoCollection collection)
    {
        IDbConnection connection = new SqlConnection(connectionString);
        using (connection)
        {
            connection.Open();
            IDbCommand command = connection.CreateCommand();
            command.CommandText = 
@"
INSERT INTO ToDoCollection (Title)
VALUES (@Title)
";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Title";
            parameter.Value = collection.Title;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }
    }

    public void Update(ToDoCollection collection)
    {
        IDbConnection connection = new SqlConnection(connectionString);
        using (connection)
        {
            connection.Open();
            IDbCommand command = connection.CreateCommand();
            command.CommandText = 
@"
UPDATE ToDoCollection
SET
    Title=@Title
WHERE Id=@Id
";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Title";
            parameter.Value = collection.Title;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = collection.Id;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }
    }

    public void Delete(int id)
    {
        throw new NotImplementedException();
    }
}