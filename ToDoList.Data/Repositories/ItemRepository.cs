using ToDoList.Data.Entities;

namespace ToDoList.Data.Repositories;

public class ItemRepository : IItemRepository
{
    public List<Item> GetAll()
    {
        throw new NotImplementedException();
    }

    public Item Get(int id)
    {
        throw new NotImplementedException();
    }

    public void Insert(Item item)
    {
        throw new NotImplementedException();
    }

    public void Update(Item item)
    {
        throw new NotImplementedException();
    }

    public void Delete(Item item)
    {
        throw new NotImplementedException();
    }
}