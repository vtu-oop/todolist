using ToDoList.Data.Entities;

namespace ToDoList.Data.Repositories;

public interface IItemRepository
{
    List<Item> GetAll();
    Item Get(int id);
    void Insert(Item item);
    void Update(Item item);
    void Delete(int id);
}