namespace ToDoList.Data.Entities;

public class ToDoCollection
{
    public int Id { get; set; }
    public string Title { get; set; }
}